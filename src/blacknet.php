<?php

namespace Blacknet\Lib;

use Blacknet\Lib\Exception\BlacknetException;
use Blacknet\Lib\Core\Compat;
use Blacknet\Lib\Core\Chacha20;
use Blacknet\Lib\Core\Bip39\Bip39;
use Blacknet\Lib\Core\Hash;
use function Blacknet\Lib\Core\ConvertPublicKey;
use function Blacknet\Lib\Core\ConvertPrivateKey;

const sigma = "Blacknet Signed Message:\n";
const hrp   = "blacknet";

/**
 * @return string - 12 word string
 */
function newMnemonic()
{
    $a = Hash::empty();
    $a->bytes();
    return Bip39::newMnemonic();
}

/**
 * @param string $account - str
 * @return array - [] 32bit
 * @throws BlacknetException
 */
function toPublickey($account)
{
    list ($gotHRP, $address) = \Bitwasp\Bech32\decode($account);
    if(strcmp($gotHRP, hrp) !== 0){
        throw new BlacknetException('Invalid hrp');
    }
    $decoded = \BitWasp\Bech32\convertBits($address, count($address), 5, 8, false);
    $program = '';
    foreach ($decoded as $char) {
        $program .= chr($char);
    }
    return $program;
}

/**
 * @param string $str - str
 * @return array - [] 32bit
 */
function hash($str)
{
    return Compat::crypto_generichash($str);
}

/**
 * @param string $mnemonic - mnemonic string
 * @param string $message - message string
 * @return string - hexstring toupper
 */
function sign($mnemonic, $message)
{
    $hashMessage = hash(sigma.$message);
    $keypair = Compat::crypto_sign_seed_keypair(hash($mnemonic));
    $sk = Compat::crypto_sign_secretkey($keypair);
    // $pk = Compat::crypto_sign_publickey($keypair);
    $signature = bin2hex(Compat::crypto_sign($hashMessage, $sk));
    return strtoupper($signature);
}

/**
 * @param string $account - account string
 * @param string $signature - signature string
 * @param string $message - message string
 * @return boolean - true/false
 */
function verify($account, $signature, $message)
{
    $hashMessage = hash(sigma.$message);
    $pk = toPublickey($account);
    $sign = hex2bin($signature);
    return Compat::crypto_sign_verify($sign, $hashMessage, $pk);
}

/**
 * @param string $mnemonic - mnemonic string
 * @param string $serialized - serialized string
 * @return string - hex string
 */
function signature($mnemonic, $serialized)
{
    $keypair = Compat::crypto_sign_seed_keypair(hash($mnemonic));
    $sk = Compat::crypto_sign_secretkey($keypair);
    $signedMessage = hex2bin(strtolower($serialized));
    $signedHash = hash(substr($signedMessage, 64));
    return bin2hex(
        substr_replace(
            $signedMessage, 
            substr(Compat::crypto_sign($signedHash, $sk),0, 64), 
            0,
            64
        )
    );
}

/**
 * @param string $mnemonic - mnemonic string
 * @param string $account - account string
 * @param string $encrypt - encrypt string
 * @return string - hex upper string
 */
function encrypt($mnemonic, $account, $encrypt)
{
    $pk = toPublickey($account);
    $keypair = Compat::crypto_sign_seed_keypair(hash($mnemonic));
    $sk = Compat::crypto_sign_secretkey($keypair);
    $publicKey = ConvertPublicKey($pk);
    $privateKey = ConvertPrivateKey($sk);
    $sharedkey = \Sodium\crypto_scalarmult($privateKey, $publicKey);
    $key = hash($sharedkey);
    $iv = random_bytes(12);
    $encrypted = Chacha20::encrypt($key, $iv, $encrypt);
    return strtoupper(bin2hex($iv.$encrypted));
}

/**
 * @param string $mnemonic - mnemonic string
 * @param string $account - account string
 * @param string $encrypt - encrypt string
 * @return string - string
 */
function decrypt($mnemonic, $account, $encrypt)
{
    $pk = toPublickey($account);
    $keypair = Compat::crypto_sign_seed_keypair(hash($mnemonic));
    $sk = Compat::crypto_sign_secretkey($keypair);
    $publicKey = ConvertPublicKey($pk);
    $privateKey = ConvertPrivateKey($sk);
    $sharedkey = \Sodium\crypto_scalarmult($privateKey, $publicKey);
    $key = hash($sharedkey);
    $encryptByte = hex2bin(strtolower($encrypt));
    $iv = substr($encryptByte,0,12);
    return Chacha20::decrypt($key, $iv, substr($encryptByte, 12));
}
