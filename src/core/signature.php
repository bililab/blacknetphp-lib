<?php

namespace Blacknet\Lib\Core;
// use SplFixedArray;

class Signature extends Utils{
    var $signature; //array
    function __construct( array $signature ) {
        if (!empty($signature)) {
            $this->signature = $signature;
        }else{
            $this->signature = array_fill(0,64,0);
        }
    }
    public function bytes(){
        return $this->signature;
    }
    public function string(){
        return self::arrayToString($this->signature);
    }
    public function length(){
        return count($this->signature);
    }
    public static function empty(){
        return new Hash(array_fill(0,64,0));
    }
}