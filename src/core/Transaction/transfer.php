<?php

namespace Blacknet\Lib\Core\Transaction;
use Blacknet\Lib\Core\Utils;
use Blacknet\Lib\Core\Message;

class Transfer extends Utils{
    var $amount; //uint64
    var $to; //string
    var $message; //Message
    function __construct($amount, $to, Message $message) {
        $this->amount  = intval($amount);
        $this->to      = self::publickeyToHex(self::publickey($to));
        $this->message = $message;
    }
    public function serialize(){
        $amount = self::toUint64Array($this->amount);
        $to = self::stringToArray(self::hexToPublickey($this->to));
        $message = $this->message->serialize();
        return array_merge(
            $amount,
            $to,
            $message
        );
    }
    public static function derialize(array $arr){
        $amount = self::uint64ArrayToNumeric(array_slice($arr, 0, 8));
        $to = self::publickeyToHex(self::arrayToString(array_slice($arr, 8, 32)));
        $message = Message::derialize(array_slice($arr, 40));
        return new Transfer($amount, $to, $message);
    }
}
