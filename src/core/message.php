<?php

namespace Blacknet\Lib\Core;
// use SplFixedArray;

class Message extends Utils{
    var $type; //uint8
    var $message; //stirng
    function __construct($type, $message) {
        $this->type    = $type;
        $this->message = $message;
    }
    public function serialize(){
        $type = [$this->type];
        $message = self::stringToArray($this->message);
        return array_merge(
            $type,
            self::encodeVarInt(count($message)),
            $message
        );
    }
    public static function derialize(array $arr){
        $type = bindec(self::arrayToString(array_slice($arr, 0, 1)));
        $len = self::decodeVarInt(array_slice($arr, 1));
        $message = self::arrayToString(array_slice($arr, count($arr) - $len));
        return new Message($type, $message);
    }
    public function empty(){
        return new Message(self::$PLAIN, '');
    }

    public static $PLAIN = 0; //uint8
    public static $ENCRYPTED = 1; //uint8
}
